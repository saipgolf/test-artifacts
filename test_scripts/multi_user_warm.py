import time
from locust import HttpUser, task, between
from locust.user.wait_time import constant, constant_pacing

class QuickstartUser(HttpUser):
    wait_time = constant_pacing(3)
    
    @task
    def function_1(self):
        self.client.get("/credit")    