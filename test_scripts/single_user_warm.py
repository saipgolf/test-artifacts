﻿import time
from locust import HttpUser, task, between
from locust.user.wait_time import constant

class QuickstartUser(HttpUser):
    wait_time = constant(1)
    
    @task
    def function_1(self):
        self.client.get("/credit")    