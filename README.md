# Test artifacts from performance testing with Locust
Install Locust (https://docs.locust.io/en/stable/installation.html) and run tests in headless mode from command line
## Amazon Web Services
```
locust -f test_scripts/single_user_cold.py 
--users 1 --spawn-rate 1 --run-time 2h --headless
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_single_user_cold.htm --csv google_single_user_cold_
 
locust -f test_scripts/single_user_warm.py 
--users 1 --spawn-rate 1 --run-time 10m --headless 
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_single_user_warm.htm --csv google_single_user_warm_
 
locust -f test_scripts/multi_user_cold.py 
--users 250 --spawn-rate 100 --run-time 1m --headless
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_multi_user_cold.htm --csv google_multi_user_cold_
 
locust -f test_scripts/multi_user_warm.py 
--users 250 --spawn-rate 100 --run-time 1m --headless 
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_multi_user_warm.htm --csv google_multi_user_warm_
```

## Google Cloud Functions
```
locust -f test_scripts/single_user_cold.py 
--users 1 --spawn-rate 1 --run-time 2h --headless
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_single_user_cold.htm --csv google_single_user_cold_
 
locust -f test_scripts/single_user_warm.py 
--users 1 --spawn-rate 1 --run-time 10m --headless 
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_single_user_warm.htm --csv google_single_user_warm_
 
locust -f test_scripts/multi_user_cold.py 
--users 250 --spawn-rate 100 --run-time 1m --headless
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_multi_user_cold.htm --csv google_multi_user_cold_
 
locust -f test_scripts/multi_user_warm.py 
--users 250 --spawn-rate 100 --run-time 1m --headless 
--host https://europe-west1-my-todo-app-313712.cloudfunctions.net 
--html google_multi_user_warm.htm --csv google_multi_user_warm_
```

## Cloud Flare
```
locust -f test_scripts/single_user_cold.py 
--users 1 --spawn-rate 1 --run-time 2h --headless
--host https://todo-app.saip.workers.dev/ 
--html cflare_single_user_cold.htm --csv cflare_single_user_cold_
 
locust -f test_scripts/single_user_warm.py 
--users 1 --spawn-rate 1 --run-time 10m --headless 
--host https://todo-app.saip.workers.dev/ 
--html cflare_single_user_warm.htm --csv cflare_single_user_warm_
 
locust -f test_scripts/multi_user_cold.py 
--users 250 --spawn-rate 100 --run-time 1m --headless
--host https://todo-app.saip.workers.dev/ 
--html cflare_multi_user_cold.htm --csv cflare_multi_user_cold_
 
locust -f test_scripts/multi_user_warm.py 
--users 250 --spawn-rate 100 --run-time 1m --headless 
--host https://todo-app.saip.workers.dev/ 
--html cflare_multi_user_warm.htm --csv cflare_multi_user_warm_
```
